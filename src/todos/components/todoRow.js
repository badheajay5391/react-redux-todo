import React, {Component} from 'react';

import {Button, Table} from 'semantic-ui-react'

const TodoRow = (props) => {
    console.log(props, 'props')
    return (
        <Table.Row >
            <Table.Cell>{props.todo.title}</Table.Cell>
            <Table.Cell>{props.todo.description}</Table.Cell>
            <Table.Cell>{props.todo.date}</Table.Cell>
            <Table.Cell>{props.todo.status}</Table.Cell>
            <Table.Cell className="options">
                <Button className="option-buttons" color='blue' onClick={props.startEditing}>
                    <i className="fa fa-pencil"></i>
                </Button>
                <Button className="option-buttons" color='red' onClick={props.deleteTodo}>
                    <i className="fa fa-trash"></i>
                </Button>
            </Table.Cell>
        </Table.Row>
    );
}

export default TodoRow;