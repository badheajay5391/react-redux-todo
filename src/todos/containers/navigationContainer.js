import React, { Component } from 'react'
import { Menu } from 'semantic-ui-react'
import { Link } from 'react-router-dom'

export default class NavigationContainer extends Component {
  state = {}

  handleItemClick = (e, { name }) => this.setState({ activeItem: name })

  render() {
    const { activeItem } = this.state

    return (
      <Menu>
        <Menu.Item
          name='todos'
          active={activeItem === 'todos'}
          onClick={this.handleItemClick}
        >
          <Link to='/'>ALL Todos</Link>
        </Menu.Item>

        <Menu.Item name='pending' active={activeItem === 'pending'} onClick={this.handleItemClick}>
         <Link to='pending'> Pending </Link>
        </Menu.Item>

        <Menu.Item
          name='inProgress'
          active={activeItem === 'inProgress'}
          onClick={this.handleItemClick}
        >
          <Link to='inprogress'>Inprogress</Link>
        </Menu.Item>
        <Menu.Item
          name='complete'
          active={activeItem === 'complete'}
          onClick={this.handleItemClick}
        >
          <Link to='complete'>Complete </Link>
        </Menu.Item>
      </Menu>
    )
  }
}