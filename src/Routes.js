import React, { Component } from 'react';
import { BrowserRouter, Switch, Route, Link } from 'react-router-dom'

import TodoContainer from './todos/containers/todoContainer'
import TodoPendingContainer from './todos/containers/todoPendingContainer'
import TodoInProgressContainer from './todos/containers/todoInProgressContainer'
import TodoCompleteContainer from './todos/containers/todoCompleteContainer'
import NavigationContainer from './todos/containers/navigationContainer'

const Routes = (props) => {
    return (
        <BrowserRouter>
        	<div>
        		<NavigationContainer />
		        <Switch>
                    <Route exact path="/" component={TodoContainer} />
                    <Route exact path="/pending" component={TodoPendingContainer} />
                    <Route path="/inprogress" component={TodoInProgressContainer} />
		            <Route path="/complete" component={TodoCompleteContainer} />
		        </Switch>
            </div>
        </BrowserRouter>
    )
}

export { Routes }